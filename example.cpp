#include <iostream>

#include "fsm.h"

// States
struct Positive {};
struct Negative {};
struct PausedNegative {};
struct PausedPositive {};
struct Faulted {};

// Inputs
struct Pause {};
struct Resume {};
struct Reset {};

std::ostream& operator<<(std::ostream &os, Pause) {
  return os << "Pause";
}

std::ostream& operator<<(std::ostream &os, Resume) {
  return os << "Resume";
}


template<typename TInput>
bool noAction(TInput &&) { return true; }
bool noAction(int &&) { return true; }

template<typename TInput>
auto lessThan(TInput &&limit) {
  return [limit=std::move(limit)](TInput const&input){ return input < limit; };
}

template<typename TInput>
auto greaterThan(TInput &&limit) {
  return [limit=std::move(limit)](TInput const&input){ return input > limit; };
}



using namespace xlr8::automata;

class SignChangeCounter : public StateMachine<
  SignChangeCounter,
  StateTypes<
    Positive,
    Negative,
    PausedNegative,
    PausedPositive,
    Faulted>,
  InputTypes<
    int,
    Pause,
    Reset,
    Resume>> {
  
public:
  SignChangeCounter() : StateMachine(StateType<Positive>()) {

    inState<Positive>()
      // Use lambdas for actions or conditions
      .on<int>([&](int const &input){ return input < 0; }).goTo<Negative>([this](int &&){ return increaseSignChanges(); })
      // Use ordinary functions for actions or transitions
      .on<int>(greaterThan(0)).goTo<Positive>(noAction)
      // Use static memberfunctions for actions or conditions
      .on<int>([&](int const &input){ return input == 0; }).goTo<Positive>(triggerError)
      // Leave conditions or actions empty if there are none
      .on<Pause>().goTo<PausedPositive>(); 

    // Use non-fluent API to add transitions
    addTransition<Negative, int, Negative>(lessThan(0), [this](int &&){ return true; });
    
    inState<Negative>()
      // Use member functions for conditions and actions
      .on<int>([&](int const &input){ return input > 0; }).goTo<Positive>(&SignChangeCounter::increaseSignChanges)
      .on<int>([&](int const &input){ return input == 0; }).goTo<Negative>(triggerError)
      .on<Pause>().goTo<PausedNegative>();
    
    inState<PausedPositive>()
      .on<Resume>().goTo<Positive>()
      .on<int>().goTo<PausedPositive>(noAction); // Works with template functions and overloads

    inState<PausedNegative>()
      .on<Resume>().goTo<Negative>()
      .on<int>().goTo<PausedNegative>();
    
    inState<Faulted>()
      .on<int>().goTo<Faulted>()
      .on<Reset>().execute(&SignChangeCounter::reset);
  }

  size_t signChanges() const { return mySignChanges; }
  
private:
  bool increaseSignChanges() {
    mySignChanges++;
    return true;
  }

  template<typename TInput>
  bool increaseSignChanges(TInput &&) {
    return increaseSignChanges();
  }
  
  template<typename TInput>
  static bool triggerError(TInput &&) { return false; }

  template<typename TInput>
  bool reset(TInput &&) {
    mySignChanges = 0;
    forceTo<Positive>();
    return true;
  }

public:
  template<typename TState, typename TInput>
  bool onMissingTransition(StateType<TState>, TInput &&input) {
    // The machine can be forced into a specific state.
    std::cout << "Missing" << std::endl;
    forceTo<Faulted>();
    return false;
  }

  template<typename TState, typename TInput, typename TNextState>
  bool onTransitionError(StateType<TState>, TInput &&input, StateType<TNextState>) {
    std::cout << "Error" << std::endl;
    forceTo<Faulted>();
    return false;
  }
private:
  size_t mySignChanges{0};
};


int main() {

  SignChangeCounter counter;
  
  std::cout << "This program counts the number of sign changes in the input Press q to quit." << std::endl;
  while(std::cin && std::cin.peek() != '\0') {
    auto nextChar = std::cin.peek();
    if (std::isdigit(nextChar) || nextChar == '+' || nextChar == '-') {
      int input;
      std::cin >> input;
      counter.onInput(std::move(input));
    }
    else if (std::isalpha(nextChar)) {
      std::string command;
      std::cin >> command;
      if (command == "p") {
        counter.onInput(Pause());
      }
      else if (command == "s") {
        counter.onInput(Resume());
      }
      else if (command == "r") {
        counter.onInput(Reset());
      }
      else {
        break;
      }
    }
    else if (nextChar == '\n') {
      std::cin.ignore();
      std::cout << "The machine is" << (counter.isInState<Faulted>() ? "" : " not") << " in state 'Faulted'.\n" 
                << "Counted " << counter.signChanges() << " sign changes so far.\n";
    }
    else {
      std::cin.ignore();
    }
  }

  // The machine can be checked for a specific state
  std::cout << "The machine is" << (counter.isInState<Faulted>() ? "" : " not") << " in state 'Faulted'.\n" 
            << "Counted " << counter.signChanges() << " sign changes in input.\n";
  
  return 0;
}
