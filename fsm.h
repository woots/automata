/*
  MIT License
  
  Copyright (c) 2023 Orlando Wingbrant

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice (including the next
  paragraph) shall be included in all copies or substantial portions of the
  Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
  IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef XLR8_AUTOMATA_H
#define XLR8_AUTOMATA_H

#include <array>
#include <functional>
#include <map>
#include <memory>
#include <vector>

namespace xlr8 {
namespace automata {

// Meta function to get the index of the first T in the pack TTypes
template<typename T, typename ...TTypes>
struct get_index_of {};

template<typename T, typename THead, typename ...TTail>
struct get_index_of<T, THead, TTail...> : std::integral_constant<std::size_t, get_index_of<T, TTail...>::value+1> {};

template<typename T, typename ...TTail>
struct get_index_of<T, T, TTail...> : std::integral_constant<std::size_t, 0> {};


// Meta function to get index of T in a tuple like class 
template <typename T, typename TTypeCollection>
struct get_index;

template <typename T, template <typename...> class C, typename ... TTypes>
struct get_index<T, C<TTypes...>> : get_index_of<T, TTypes...> {};


// Base class for transitions
class Transition {
public:
  virtual ~Transition() = default;
};

// Class that represents a transition on type TInput
template<typename TInput>
class TransitionOn : public Transition {
public:
  ~TransitionOn() override = default;
  TransitionOn(
    std::size_t nextState,
    std::function<bool(TInput &&)> &&transitionFunction,
    std::function<bool(TInput &&)> &&errorFunction,
    std::function<bool(TInput const&)> &&condition = [](TInput const&){ return true;},
    bool ignoreTransitionResult = false)
    : myNextState(nextState),
      myCondition(std::move(condition)),
      myTransitionFunction(std::move(transitionFunction)),
      myErrorFunction(std::move(errorFunction)),
      myIgnoreResult(ignoreTransitionResult) {}

  // Can be used to determine if transition is to be taken or not for a given input.
  bool isValidFor(TInput const &input) const { return myCondition(input); }

  // Returns true if the transition is for a missing transition.
  bool ignoreResult() { return myIgnoreResult; }
  
  // Returns the index of the state this transition leads into.
  size_t nextState() { return myNextState; }
  
  // Invoke the transition action function.
  bool on(TInput&& input) const { return myTransitionFunction(std::forward<TInput>(input)); }

  // Invoke the error transition action function.
  bool onError(TInput&& input) const { return myErrorFunction(std::forward<TInput>(input)); }

private:
  std::size_t myNextState;
  std::function<bool(TInput const&)> myCondition;
  std::function<bool(TInput&&)> myTransitionFunction;
  std::function<bool(TInput&&)> myErrorFunction;
  bool myIgnoreResult;
};


// Class to group state types in a machine
template<typename ...TStates>
class StateTypes {};


// Class to group input types to a machine
template<typename ...TInputTypes>
class InputTypes {};


// Class that represents the current state type
template<typename T>
class StateType {};


namespace internal {
namespace transition {

  template<typename TStateMachine, typename TState, typename TInput>
class NextStateDefinition;

template<typename TStateMachine, typename TState>
class InputDefinition {
public:
  InputDefinition(TStateMachine *sm) : myMachine(sm) {}
  
  // Define input without condition
  template<typename TInput>
  auto on() -> NextStateDefinition<TStateMachine, TState, TInput> {
    return on<TInput>(std::function<bool(TInput const&)>([](TInput const&){ return true; }));
  };

  // Define input with external function condition
  template<typename TInput>
  auto on(bool(*condition)(TInput const&)) -> NextStateDefinition<TStateMachine, TState, TInput> {
    return NextStateDefinition<TStateMachine, TState, TInput>(
      myMachine,
      [condition](TInput const &input){ return (*condition)(input); });
  };
  
  // Define input with class member function condition
  template<typename TInput>
  auto on(bool(TStateMachine::*condition)(TInput const&)) -> NextStateDefinition<TStateMachine, TState, TInput> {
    return NextStateDefinition<TStateMachine, TState, TInput>(
      myMachine,
      [sm=myMachine, condition](TInput const &input){ return (sm->*condition)(input); });
  };
  
  // Define input with functor condition
  template<typename TInput>
  auto on(std::function<bool(TInput const&)> &&condition) -> NextStateDefinition<TStateMachine, TState, TInput> {
    return NextStateDefinition<TStateMachine, TState, TInput>(
      myMachine,
      std::move(condition));
  };
  
private:
  TStateMachine *myMachine;
  //  size_t myStateIndex;
};

  template<typename TStateMachine, typename TState, typename TInput>
class NextStateDefinition {
public:
  NextStateDefinition(
    TStateMachine *sm,
    std::function<bool(TInput const&)> &&condition)
    : myMachine(sm), myTransitionCondition(std::move(condition)) {}


  // Define transition without an action
  template<typename TNextState>
  auto goTo() -> InputDefinition<TStateMachine, TState> {
    return goTo<TNextState>(std::function<bool(TInput&&)>([](TInput&&){ return true; }));
  }

  // Define transition  with an external function action
  template<typename TNextState>
  auto goTo(bool(*action)(TInput &&)) -> InputDefinition<TStateMachine, TState> {
    return goTo<TNextState>([action](TInput &&input){ return action(std::forward<TInput>(input)); });
  }

  // Define transition with a class member function action
  template<typename TNextState>
  auto goTo(bool(TStateMachine::*action)(TInput &&)) -> InputDefinition<TStateMachine, TState> {
    return goTo<TNextState>([sm=myMachine, action](TInput &&input){ return (sm->*action)(std::forward<TInput>(input)); });
  }

  // Define transition  with a functor action
  template<typename TNextState>
  auto goTo(std::function<bool(TInput &&)> &&action) -> InputDefinition<TStateMachine, TState> {
    myMachine->template addTransition<TState, TInput, TNextState>(std::move(myTransitionCondition), std::move(action));
    return InputDefinition<TStateMachine, TState>(myMachine);
  }


  // Define transition and end state with a functor action
  auto execute(std::function<bool(TInput &&)> &&action) -> InputDefinition<TStateMachine, TState> {
    myMachine->template addTransition<TState, TInput>(std::move(myTransitionCondition), std::move(action));
    return InputDefinition<TStateMachine, TState>(myMachine);
  }

  // Define transition and end state with an external function action
  auto execute(bool(*action)(TInput &&)) -> InputDefinition<TStateMachine, TState> {
    return execute([action](TInput &&input){ return action(std::forward<TInput>(input)); });
  }

  // Define transition and end state with a class member function action
  auto execute(bool(TStateMachine::*action)(TInput &&)) -> InputDefinition<TStateMachine, TState> {
    return execute([sm=myMachine, action](TInput &&input){ return (sm->*action)(std::forward<TInput>(input)); });
  }


private:
  TStateMachine *myMachine;
  std::function<bool(TInput const&)> myTransitionCondition;
};


} // namespace transition
} // namespace internal
  
// State machine base template.
  template<typename TSelf, typename TStateTypes, typename TInputTypes>
class StateMachine;

// State machine class. TSelf must be a class that contain function
// definitions for all combinations of TStateTypes and TInputTypes like
//    bool onMissingTransition(CurrentStateType<State>, Input &&);
// and
//    bool onErrorTransition(CurrentStateType<State>, Input &&);
template<typename TSelf, typename ...TStateTypes, typename ...TInputTypes>
class StateMachine<TSelf, StateTypes<TStateTypes...>, InputTypes<TInputTypes...>> {

  using StateTps = std::tuple<TStateTypes...>;
  using InputTps = std::tuple<TInputTypes...>;

  using TransitionTable =
    std::array<
      std::array<
        std::vector<std::unique_ptr<Transition>>,
        std::tuple_size<StateTps>::value>,
    std::tuple_size<InputTps>::value>;

  template<typename TInput, typename TState>
  auto makeTransition() -> std::vector<std::unique_ptr<Transition>> {
    std::vector<std::unique_ptr<Transition>> tbl;
    // Last entry holds a function that will call TSelf::onMissingTransition()
    tbl.emplace_back(new TransitionOn<TInput>(
        get_index<TState, StateTps>::value,
        [&](TInput &&input){
          return static_cast<TSelf*>(this)
            ->onMissingTransition(StateType<TState>(), std::forward<TInput>(input));
        },
        [](TInput &&input){ return false; },
        [](TInput const&){ return true;},
        true));
    return tbl;
  };
  
  template<typename TInputTp, typename ...TStateTps>
  auto makeRow()
    -> std::array<std::vector<std::unique_ptr<Transition>>, sizeof...(TStateTps)> {
    return std::array<
      std::vector<std::unique_ptr<Transition>>,
      sizeof...(TStateTps)>{ makeTransition<TInputTp, TStateTps>()... };
  };

  auto initTable() -> TransitionTable {
    return TransitionTable{ makeRow<TInputTypes, TStateTypes...>()...};
  };

public:
  virtual ~StateMachine() = default;
  
  template<typename TInitialState, std::size_t N = get_index<TInitialState, StateTps>::value>
  explicit StateMachine(StateType<TInitialState>)
    : myCurrentState(N),
      myTransitionTable(initTable()) {}

  // Make transition on input.
  template<typename TInput>
  auto onInput(TInput &&input) -> bool {
    auto inputIndex = get_index<TInput, InputTps>::value;
    TransitionOn<TInput> *transition = nullptr;
    for (auto &entry : myTransitionTable[inputIndex][myCurrentState]) {
      transition = static_cast<TransitionOn<TInput>*>(entry.get());
      if (transition->isValidFor(input)) {
        break;
      }
    }
    bool result = false;
    if (transition->ignoreResult()) {
      result = transition->on(std::forward<TInput>(input));
    }
    else if (transition->on(std::forward<TInput>(input))) {
      myCurrentState = transition->nextState();
      result = true;
    }
    else {
      result = transition->onError(std::forward<TInput>(input));
    }
    return result;
  }

  // Force state machine into specific state
  template<typename TState>
  void forceTo() {
    myCurrentState = get_index<TState, StateTps>::value;
  }

  // Check if state machine is in a specific state
  template<typename TState>
  bool isInState() {
   return  myCurrentState == get_index<TState, StateTps>::value;
  }

  // Fluently add a transition to the state machine.
  template<typename TState>
  auto inState() {
    return internal::transition::InputDefinition<TSelf, TState>(
      static_cast<TSelf*>(this));
  }
  
  // Add a new transition to the state machine with a condition on the input
  // and a transition action.
  template<typename TState, typename TInput, typename TNextState>
  auto addTransition(
    std::function<bool(TInput const &input)> &&condition,
    std::function<bool(TInput &&input)> &&action)
    -> void {
    auto stateIndex = get_index<TState, StateTps>::value;
    auto inputIndex = get_index<TInput, InputTps>::value;
    myTransitionTable[inputIndex][stateIndex].insert(
      myTransitionTable[inputIndex][stateIndex].begin(),
      std::unique_ptr<Transition>(new TransitionOn<TInput>(
          get_index<TNextState, StateTps>::value,
          std::move(action),
          [this](TInput &&input){ return static_cast<TSelf*>(this)
              ->onTransitionError(StateType<TState>(), std::move(input), StateType<TNextState>()); },
          std::move(condition))));
  }


  // Add a new transition to the state machine with a condition on the input
  // and a transition action with open end state.
  template<typename TState, typename TInput>
  auto addTransition(
    std::function<bool(TInput const &input)> &&condition,
    std::function<bool(TInput &&input)> &&action)
    -> void {
    auto stateIndex = get_index<TState, StateTps>::value;
    auto inputIndex = get_index<TInput, InputTps>::value;
    myTransitionTable[inputIndex][stateIndex].insert(
      myTransitionTable[inputIndex][stateIndex].begin(),
      std::unique_ptr<Transition>(new TransitionOn<TInput>(
          stateIndex,
          std::move(action),
          [](TInput &&){ return false; },
          std::move(condition),
          true)));
  }
  
private:
  std::size_t myCurrentState;
  TransitionTable myTransitionTable;  
};

  
} // namespace automata
} // namespace xlr8

#endif
